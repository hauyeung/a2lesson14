package com.example.a2lesson14;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class ParseXML {

	public String title = "title";
	public String link = "link";
	public String description = "description";

	public String urlString = null;	
	public volatile boolean parsingComplete = true;


	public ArrayList<String[]> parseXML(String urlstring) {
		ArrayList<String[]> rssentries = new ArrayList<String[]>();
		String[] rssentry = new String[3];		
		String text=null;
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();		
			URL url = new URL(urlstring);
			HttpURLConnection conn = (HttpURLConnection) 
					url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();
			InputStream stream = conn.getInputStream();			
			parser.setInput(stream, null);

			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagname = parser.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					if (tagname.equalsIgnoreCase("item")) {
						// create a new instance of employee
						rssentry = new String[3];
					}
					break;

				case XmlPullParser.TEXT:
					text = parser.getText();
					break;

				case XmlPullParser.END_TAG:
					if (tagname.equalsIgnoreCase("item")) {						// 
						rssentries.add(rssentry);
					} else if (tagname.equalsIgnoreCase("title")) {
						rssentry[0] = text;
						System.out.println(text);
					} else if (tagname.equalsIgnoreCase("link")) {
						rssentry[1] = text;
//						System.out.println(text);
					} else if (tagname.equalsIgnoreCase("description")) {
						rssentry[2] = text;
//						System.out.println(text);
					}
					break; 
				default:
					break;
				}
				eventType = parser.next();
			}
		} catch (Exception e) {

		}
		return rssentries;
	}
}




