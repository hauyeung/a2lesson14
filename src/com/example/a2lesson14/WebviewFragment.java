package com.example.a2lesson14;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class WebviewFragment extends Fragment{
	private WebView webview;
	private String url;
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(
				R.layout.webviewfragment, container, false);

		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		webview = (WebView) getView().findViewById(R.id.webview);
		webview.setWebChromeClient(new WebChromeClient(){
			
		});
		
		webview.setWebViewClient(new WebViewClient(){
			
		});
		Bundle b = getArguments();
		url = b.getString("url");
		webview.loadUrl(url);
		if (savedInstanceState != null)
		{
			url = savedInstanceState.getString("url");			
		}
		
		int ori = getResources().getConfiguration().orientation;
		if (ori == Configuration.ORIENTATION_PORTRAIT)
		{
			Button back = (Button) getView().findViewById(R.id.backbutton);
			back.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					MainActivity ma = (MainActivity) getActivity();
					ma.loadrssfragment();
				}
				
			});
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("url", url);
	}
}
