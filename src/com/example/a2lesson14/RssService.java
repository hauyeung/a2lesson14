package com.example.a2lesson14;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class RssService extends Service{

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {


		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setSmallIcon(R.drawable.ic_launcher);		
		mBuilder.setContentTitle("New item is available");
		mBuilder.setContentText("New item is available");		
		NotificationManager mNotificationManager =
				(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification b = mBuilder.build();
		PendingIntent notificationIntent = TaskStackBuilder.create(this).addNextIntent(new Intent()).getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		b.contentIntent = notificationIntent ;
		// notificationID allows you to update the notification later on.
		mNotificationManager.notify(0, b);


		return Service.START_NOT_STICKY;
	}

}
