package com.example.a2lesson14;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

public class MainActivity extends FragmentActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.rsscontainer, new RssFragment());
		ft.commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void loadwebviewfragment(String url)
	{
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();		
		int ori = getResources().getConfiguration().orientation;
		Bundle b = new Bundle();
		b.putString("url", url);
		WebviewFragment wf = new WebviewFragment();
		wf.setArguments(b);
		if (ori == Configuration.ORIENTATION_LANDSCAPE)
		{
			ft.replace(R.id.webviewcontainer, wf);
		}
		else if (ori == Configuration.ORIENTATION_PORTRAIT)
		{
			ft.replace(R.id.rsscontainer, wf);
		}

		ft.commit();
	}

	public void loadrssfragment()
	{
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		int ori = getResources().getConfiguration().orientation;
		if (ori == Configuration.ORIENTATION_PORTRAIT)
		{
			ft.replace(R.id.rsscontainer, new RssFragment());
		}

		ft.commit();
	}

	public void startrssservice()
	{
		startService(new Intent(MainActivity.this, RssService.class));
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				stopService(new Intent(MainActivity.this, RssService.class));
			}
		}, 10000);
	}




}
