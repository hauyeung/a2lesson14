package com.example.a2lesson14;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class RssFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<String[]>> {

	private ArrayAdapter<String> rssadapter;	
	private ListView rsslistview; 
	private EditText urlfield;
	private Button viewbutton;		
	private ArrayList<String> rsstitles = new ArrayList<String>();
	private static String url;
	private  Loader<ArrayList<String[]>> loader;
	private ArrayList<String[]> rssentries;
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {	
		loader = getActivity().getSupportLoaderManager().initLoader(0,null,this);
		return inflater.inflate(
				R.layout.rssfragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);

		urlfield = (EditText) getView().findViewById(R.id.url);
		viewbutton  = (Button) getView().findViewById(R.id.viewbutton);
		rsslistview = (ListView) getView().findViewById(R.id.rsslistview);
		rssadapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.rsstextview, rsstitles);
		rsslistview.setAdapter(rssadapter);
		viewbutton.setOnClickListener(new OnClickListener(){			

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				url = urlfield.getText().toString();	
				loader.forceLoad();
			}

		});
		
		rsslistview.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				MainActivity ma = (MainActivity) getActivity();
				ma.loadwebviewfragment(rssentries.get(position)[1]);
			}
			
		});
		
	

	}

	@Override
	public Loader<ArrayList<String[]>> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return new RSSLoader(this.getActivity());
	}


	@Override
	public void onLoaderReset(Loader<ArrayList<String[]>> arg0) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onLoadFinished(Loader<ArrayList<String[]>> loader,
			ArrayList<String[]> data) {
		// TODO Auto-generated method stub
		int oldsize = rsstitles.size();
		if (oldsize > 0)
		{
			rsstitles = new ArrayList<String>();
		}
		try{
			rssentries = data;				
			for (String[] rss: rssentries)
			{
				rsstitles.add(rss[0]);		
				
			}
			rssadapter.notifyDataSetChanged();
		}
		catch (Exception e)
		{
			
		}
		if (oldsize < rsstitles.size())
		{
			MainActivity ma = (MainActivity) getActivity();
			ma.startrssservice();
		}
	}


	private static class RSSLoader extends AsyncTaskLoader<ArrayList<String[]>> {


		public RSSLoader(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}


		@Override
		public ArrayList<String[]> loadInBackground() {
			ArrayList<String[]> rssentries = null;
			try {
				System.out.println("Parse XML");
				ParseXML pxml = new ParseXML();						
				rssentries  = pxml.parseXML(url);			
			} catch (Exception e) {
			}

			return rssentries;
		}
	}



}
